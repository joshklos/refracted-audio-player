var my_audio_files = document.getElementsByTagName("audio");
var my_index = 0;
for (var i=0; i<my_audio_files.length; i++)
{
	my_index += 1;
	audio_file = my_audio_files[i];
	audio_file.id = "audio-" + my_index;
    audio_file.classList.add("hide");
	audio_file.insertAdjacentHTML('afterend', buildAudioControls(my_index));

	audio_file.addEventListener('durationchange', updateduration);
	audio_file.addEventListener('timeupdate', updatetime);

	var play_button = document.getElementById('play-audio-' + my_index);
	play_button.addEventListener('click', playpause);
    var my_controls = document.getElementById('audio-controls-' + my_index);
    my_controls.addEventListener("click", moveplayhead);

}

function moveplayhead(event)
{
    var my_horizontal_mouse = event.clientX;
    var my_adjusted_horizontal = my_horizontal_mouse - 148;
    var my_progress_bar = event.target.getElementsByClassName('audio-progress')[0];
    var my_time_section = event.target.getElementsByClassName('audio-time')[0];

    try {
        var my_width =  my_time_section.offsetLeft-20-my_progress_bar.offsetLeft;
    } catch(e) {return;}

    if (my_adjusted_horizontal > my_width)
        {
            var my_start = my_width;
        }
    else if (my_adjusted_horizontal > 0)
        {
            var my_start = my_adjusted_horizontal/my_width;
        }
    else var my_start = 0;

    var my_audio = event.target.previousSibling;
    my_audio.currentTime = my_start * my_audio.duration;

}

function updateduration(event)
{
	var my_time = document.getElementById('time-' + event.target.id);
    my_time.innerHTML = formatTime(0) + "/" + formatTime(event.target.duration);
}

function updatetime(event)
{
	var my_time = document.getElementById('time-' + event.target.id);
	my_time.innerHTML = formatTime(event.target.currentTime) + "/" + formatTime(event.target.duration);

    var my_progress_bar = document.getElementById('progress-' + event.target.id);

    var my_left_edge = my_progress_bar.offsetLeft;
    var my_right_edge = my_time.offsetLeft-20;

    var my_percentage = progress_percent(event.target.currentTime, event.target.duration);
    var my_width = my_percentage*(my_right_edge-my_left_edge);

    my_progress_bar.style.width = my_width;
}

function progress_percent(current_time, duration)
{
    return current_time/duration;
}

function playpause(event)
{
	var my_command = event.target.innerHTML;
	var my_audio_id = event.target.id.replace("play-", "");
	var my_audio = document.getElementById(my_audio_id);

	if (my_command.charCodeAt(0) == "9654")
	{
		my_audio.play();
		event.target.innerHTML = "||";
        event.target.style.fontWeight = "bold";
	}
	if (my_command == "||")
	{
		my_audio.pause();
        event.target.style.fontWeight = "normal";
		event.target.innerHTML = "&#9654;";
	}
}

function formatTime(raw_time)
{
	var my_minutes = Math.floor(raw_time/60);
	var my_seconds = Math.floor(raw_time-(my_minutes*60));

	if (my_minutes < 10)
	{
		my_minutes = "0" + my_minutes;
	}
	if (my_seconds < 10)
	{
		my_seconds = "0" + my_seconds;
	}

	return my_minutes + ":" + my_seconds;
}

function buildAudioControls(my_index)
{
    var my_colors =[
    "#7AC943",
    "#52C8EC",
    "#FF7BAC"
    ]

    if (my_index > my_colors.length)
        {
            var my_remained = my_index - (my_colors.length * (Math.floor(my_index/my_colors.length)))
            var my_color = my_colors[my_remained-1]
        }
    else var my_color = my_colors[my_index-1];

	var my_html = "<div id='audio-controls-" + my_index + "' class='audio-controls'>\n";
	my_html += '\t<div id="play-audio-';
	my_html += my_index;
	my_html += '" class="audio-play" style="background-color: ' +my_color+ '">&#9654;</div>\n';
	my_html += '\t<div class="audio-time" id="time-audio-';
	my_html += my_index;
	my_html += '">00:00/00:00</div>\n';
	my_html += '\t<div id="progress-audio-';
	my_html += my_index;
	my_html += '" style="background-color: ' +my_color+ '" class="audio-progress"></div>\n';
	my_html += "</div>";

	return my_html
}
